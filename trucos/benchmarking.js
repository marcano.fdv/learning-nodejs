
//console.time devuelve el tiempo de ejecución de todas las funciones y operaciones incluidas entre el time y timeEnd del mismo nombre
console.time('todo el codigo')
let suma = 0;
console.time('bucle');
for (i = 0; i > 1; i++){
    suma++
}
console.timeEnd('bucle');

let suma2 = 0;
console.time('bucle 2')
for (j = 0; j > 3; j++){
    suma2++
}
console.timeEnd('bucle 2');

console.time('asincrono')
console.log('Inicia la funcion async')
asincrona()
    .then(()=>{
        console.timeEnd('asincrono')
    })

console.timeEnd('todo el codigo')

function asincrona(){
    return new Promise ((resolve)=>{
        setTimeout(function(){
            console.log('Termina el proceso async');
            resolve()
        })
    })
}