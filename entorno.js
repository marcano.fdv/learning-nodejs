//variables de entorno
//para acceder a las variables de entorno se escribe process.env.VARIABLE
//desde la consola se le agrega el valor a variable seguido de node y el archivo a ejecutar ejemp NOMBRE=Francis node entorno.js
//el valor por defecto se coloca luego del or
//para definir varias variales en la consola se agregan una luego de otra dejando espacios
//BP: Colocar lasvariables de entorno en mayusculas, si son dos palabras separarlas por guion bajo

let nombre = process.env.NOMBRE || 'sin nombre';
let apellido = process.env.MI_APELLIDO || 'sin apellido';
console.log('Hola ' + nombre)
console.log('mi apellido es ' + apellido)


