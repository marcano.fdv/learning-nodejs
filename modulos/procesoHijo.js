// REVISAR LIBRERIA EN LA DOCUMENTACION
// Con la libreria child process de node js podemos gestionar y/o ejecutar procesos hijos
// Estos procesos pueden estar fuera del proceso principal
// con esto requerimos la propiedad exec del modulo child process

//Se pueden ver mas opciones de procesos hijos en la documentacion de node

const {exec, spawn} = require('child_process');

// exec('ls -la', (err, stdout, sterr)=>{
//     if(err){
//         console.error(err);
//         return false;
//     }
//
//     console.log(stdout)
// })
//
// exec('node modulos/console.js', (err, stdout, sterr)=>{
//     if(err){
//         console.error(err);
//         return false;
//     }
//
//     console.log(stdout)
// })

//Con exec podemos traer codigo a node y ejecutarlo por debajo del hilo principal

// spawn nos permite invocar un proeso nuevo de nodejs

let proceso = spawn('ls',['-la']);

console.log(proceso);
console.log(proceso.pid);
console.log(proceso.connected);

//ejemplo de uso orientado a eventos
proceso.stdout.on('data', function(dato){
    console.log(dato.toString());
    console.log('esta muerto?');
    console.log(proceso.killed);
});

proceso.on('exit', function(){
    console.log('termino')
})