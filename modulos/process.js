const process = require ('process');
//el modulo process se requiere automaticamente en global por lo que no es necesario requerir el modulo en el archivo
// desde ahora se va a trabajar con el process que afecta en global

process.on('beforeExit', ()=>{
    console.log('El proceso va a terminar');
})
process.on('exit', ()=>{
    console.log('El proceso acabo');
})

//para escuchar una excepcion que se ha pasado capturar
// process.on('uncaunghtException') o uncaunghtRejection para promesas que no tienen un try/catch

process.on('uncaughtException', (err, origen)=>{
    console.error('No se capturo este error');
    console.error(err);
})

funcionQueNoExiste();

console.log('si no capturo el error esto no se muestra');

//a partir de que se ejecute el exit ya no se podra ejecutar nada mas dentro del hilo porque se habra desconectado completamente
//a partir del exit todos los comandos que se quieran ejecutar deben ser asincronos
