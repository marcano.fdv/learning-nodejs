//El modulo de file system permite acceder a archivos para editarlos, leerlos, eliminarlos etc
//al requerir este modulo no es necesario indicar donde esta porque viene instalado por defecto en node
//
//REVISAR FUNCIONES EN LA DOUMENTACION
//
// Todos los porcesos que se realizan con fs son asincronos, aunque tienen una alternativa sincrona no se recomienda utilizarla porque
// bloquea el hilo de procesos principal

const fs = require('fs');

function leer(ruta, cb){
    fs.readFile(ruta, (error, data)=>{
        cb(data.toString());
    })
}

function escribir (ruta, contenido, cb){
    fs.writeFile(ruta, contenido, function(err){
        if(err){
            console.error('no se pudo escribir', err)
        } else{
            console.log('Se escribio correctamente')
        }
    });
}

function borrar (ruta, cb){
    fs.unlink(ruta, cb);
}

// leer(__dirname + '/archivo.txt', console.log)
// escribir(__dirname + '/ArchivoEscrito.txt', 'Soy un archivo nuevo', console.log);
borrar (__dirname + '/ArchivoEscrito.txt', console.log)