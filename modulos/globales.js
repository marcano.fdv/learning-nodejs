//REVISAR LA DOCUMENTACION EN LA WEB DE NODE
// Los modulos globales vienen predefinidos en node
//setInterval / clearInterval
//setTimeOut
//setImmediate
//require() para requerir modulos en cualquier parte del programa


// para ver el contenido del objeto global, NO SE DEBE HACER EN PROD console.log(global);

let i = 0;
let intervalo = setInterval(()=>{
    if(i===3){
        clearInterval(intervalo)
    }
    console.log('hola');
    i++;

}, 1000)


setImmediate(()=>{
    console.log('Soy inmediato')
})

//para acceder a la info del proceso
console.log(process)

//para ver la direccion del programa
console.log(__dirname);
console.log(__filename);


//------------------------

// Lo ideal es no utilizar variables globales
// En caso sea necesario la sintaxis es la siguiente

global.miVariable ="elValor";
console.log(miVariable);