// permite mostrar algo por consola
console.log('algo')
console.info()

//muestra un error con formao diferente a un log (pero puede que no)
//internamente sale a otro sitio
console.error('un error')

//para advertencias
console.warn('una advertencia')

//muestra datos en forma de tabla
var tabla = [
    {
        a:1,
        b:'z'
    },
    {
      c:3,
      d:'x'
    },
]
console.log(tabla);
console.table(tabla);

//nos permite agrupar log para definir que son de un mismo conjunto
function funcion1(){
    console.group('funcion 1');
    console.log('soy de la funcion 1');
    funcion2();
    console.log('estoy en la funcion 1');
    console.groupEnd('funcion 1')
}

function funcion2(){
    console.group('funcion 2');
    console.log('Estoy en la funcion 2')
    console.groupEnd('funcion 2');
}

funcion1();


//contador
console.count('veces');
console.count('veces');
console.count('veces');
console.countReset('veces');
console.count('veces');
