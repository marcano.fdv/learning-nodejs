//Cada vez que tenemos una funcion que podria romperse hay que meterla dentor de un try/catch

function otraFuncion(){
    errores();
}

function errores(){
    return 3 + z;
}

//ejempo de try catch en funciones asincronas
function seRompeAsincrona(cb){
    setTimeout(function(){
        try{
            return 3 + z;
        } catch(err){
            console.error('Error en mi funcion asincrona');
            cb(err);
        }
    })
}

try{
    // otraFuncion();
    seRompeAsincrona(function(err){
        console.log('hay error');
    })
}catch(err){
    console.error('Algo se rompio');
    console.error(err);
    console.error(err.message);
}

console.log('esto esta al final')