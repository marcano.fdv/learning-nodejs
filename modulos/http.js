// Este modulo nos permite crear o conectarnos a servidores externos

const http = require('http');

//para crear un nuevo servidor se utiliza el metodo create de http, este contiene una funcion que contiene los que se ejecutara dentro del servidor
//para que se ejecute el servidor debe estar escuchando en un puerto del sistema

http.createServer(router).listen(3000);

function router(request, response){
    console.log('nueva peticiòn');
    console.log(request.url);

    switch (request.url){
        case '/hola':
            response.write('Holis');
            response.end();
            break;

        default:
            response.write('error 404: Aqui no esta lo que buscas');
            response.end();
    }

    // //podemos agregar cabeceras con la informaciòn del request
    // response.writeHead(201, {
    //     'Content-Type' : 'text/plain'
    // })
    //
    // //escribe la respuesta al usuario
    // response.write('hola, ya se usar HTTP de node js')
    //
    // response.end();
}

console.log('escuchando HTTP en el puerto 3000')
//se considera una buena practica hacer un console.log indicando en que puerto se esta escuchando