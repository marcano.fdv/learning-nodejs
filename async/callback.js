//los callback son funciones que pasan como parametros
//Se utilizan para manejarla asincronia

function  hola (nombre, miCallback){
    setTimeout(()=>{
        console.log('Hola, ' + nombre);
        miCallback(nombre);
    }, 1000);
};

function adios (nombre, otroCallback){
    setTimeout(()=>{
        console.log('Adios, ' + nombre);
        otroCallback();
    }, 1000)
}

console.log ('Iniciando proceso...');

hola('Francis', (nombre)=>{
    adios(nombre, ()=>{
        console.log('Terminando proceso')
    })
})