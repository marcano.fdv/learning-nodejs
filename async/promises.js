// las promesas vienen de los callbacks, a diferencia de los mismos estas pueden tener estados como resueltas, no recueltas, pendientes y fallidas
// las promesas son una clase global asi que podemos llamarlas en el codigo
// las promesas se pueden anidar

function  hola (nombre){
    return new Promise(function(resolve, reject){
        setTimeout(()=>{
            console.log('Hola, ' + nombre);
            resolve(nombre);
        }, 1000);
    });
};

function adios (nombre){
    return new Promise ((resolve, reject)=>{
        setTimeout(()=>{
            console.log('Adios, ' + nombre);
            resolve();
        }, 1000)
    })
}

function hablar(nombre){
    return new Promise((resolve, reject)=>{
        setTimeout(function(){
            console.log('Bla bla bla bla...');
            // resolve(nombre);
            reject('Hay un error')
        },1000)
    })
}

//----
console.log('Iniciando el proceso...')

hola('Francis')
    .then(hablar)
    .then(hablar)
    .then(hablar)
    .then(adios)
    .then((nombre)=>{
        console.log('terminado el proceso');
    })
    .catch(error=>{
        console.error('Hubo un error:');
        console.error(error);
    })

// Al utilizar promesas siempre se debe colocar un catch al final
// Si alguna de las promesas llega a fallar va a enviar directamente al catch y de esta forma no se propaga el error