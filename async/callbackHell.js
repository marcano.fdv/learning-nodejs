function  hola (nombre, miCallback){
    setTimeout(()=>{
        console.log('Hola, ' + nombre);
        miCallback(nombre);
    }, 1000);
};

function adios (nombre, otroCallback){
    setTimeout(()=>{
        console.log('Adios, ' + nombre);
        otroCallback();
    }, 1000)
}

function hablar(callbackHablar){
    setTimeout(function(){
        console.log('Bla bla bla bla...');
        callbackHablar();
    },1000)
}

function conversacion(nombre, veces, callback){
    if(veces > 0){
        hablar(function(){
            conversacion(nombre, --veces, callback);
        })
    }else{
        adios(nombre, callback);
    }
}

console.log ('Iniciando proceso...');

// --

// hola('Francis', (nombre)=>{
//     hablar(function(){
//         hablar(function(){
//             hablar(function(){
//                 adios(nombre, ()=>{
//                     console.log('Terminando proceso')
//                 })
//             })
//         })
//     })
// })

//Como evitar el call back hell
// El codigo reutilizable debe ir en una funcion
// Al comenzar a trabajar con callbacks mas complejos hay que refactorizar

hola('Francis', function(nombre){
    conversacion(nombre, 3, function(){
        console.log('Proceso terminado')
    });
})