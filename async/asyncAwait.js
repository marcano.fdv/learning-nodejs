// Nos permite definir una funcion explicitamente como asincrona y esperar a que termine para continuar con los procesos que dependen de esta funcion
//a nivel tecnico no bloquea el hilo principal, los procesos que no necesiten de las funciones asincronas seguiran corriendo en paralelo

async function hola (nombre){
    return new Promise(function(resolve, reject){
        setTimeout(()=>{
            console.log('Hola, ' + nombre);
            resolve(nombre);
        }, 1000);
    });
};

async function adios (nombre){
    return new Promise ((resolve, reject)=>{
        setTimeout(()=>{
            console.log('Adios, ' + nombre);
            resolve();
        }, 1000)
    })
}

async function hablar(nombre){
    return new Promise((resolve, reject)=>{
        setTimeout(function(){
            console.log('Bla bla bla bla...');
            resolve(nombre);
            // reject('Hay un error')
        },1000)
    })
}

//await solo se puede utilizar dentro de una funcion asyncrona

async function main(){
    let nombre = await hola('Fran');
    await hablar();
    await hablar();
    await hablar();
    await adios(nombre);
    console.log('Termina proceso');
}


console.log('Inicia proceso');

main();

console.log('Soy asincrono');