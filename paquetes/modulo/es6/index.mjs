//Se recomienda utilizar los modulos nativos
//en caso de utilizar modulos de ecmascript 6 hay que utilizar uns ranspilador para que corra sin problemas
import modulo from './modulo.mjs';

//en primer momento da un error porque el modulo de ecmas6 es experimental
//para correrlo se debe realizar
//nodemon --experimental-modules paquetes/modulo/es6/index.mjs

// modulo('francis')

console.log(modulo.prop1)
modulo.saludar('francis')