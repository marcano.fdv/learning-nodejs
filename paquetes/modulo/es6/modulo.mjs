function saludar(nombre){
    console.log('hola ' + nombre);
}

//el modulo que se esta exportando puede ser tanto una funcion como un objeto

// export default saludar
export default{
    saludar,
    prop1: 'Soy un modulo experimental',
}