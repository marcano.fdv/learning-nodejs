//bcrypt funciona tanto con callbacks como con promesas
const bcrypt = require('bcrypt');

const password = '1234segura!';

bcrypt.hash(password, 5, function(err, hash){
    console.log(hash);

    bcrypt.compare(password, hash, (err, res) =>{
        console.log(res);
    })
})