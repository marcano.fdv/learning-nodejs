//Los streams se entienden como procesos que permiten sacar y adicionar poco a poco información de un fichero de gran tamaño
// Existen streams de lectura, de escritura y bidireccionales

const fs = require('fs');
const stream = require('stream');
const util = require('util');

let data = '';

let readableStream = fs.createReadStream(__dirname + '/input.txt');
//si sabemos que tipo de datos devolvera el archivo podemos utilizar setEncoding para configurar el tipo de dato desde el inicio
readableStream.setEncoding('UTF8')
readableStream.on('data', (chunk)=>{
    // console.log(chunk)
    data += chunk;
})
readableStream.on('end', ()=>{
    console.log(data);
})

//--

//stdout es un buffer de escritura que nos permite escribir directamente en la salida del sistema (consola)
process.stdout.write('Hola')
process.stdout.write('que')
process.stdout.write('tal')
//--

//Crear un buffer de transformación que lea de un fichero, convierta en mayusculas y escriba

const Transform = stream.Transform;
function Mayus(){
    Transform.call(this);
}
util.inherits(Mayus, Transform);

Mayus.prototype._transform = function (chunk, codif, cb){
    chunkMayus = chunk.toString().toUpperCase();
    this.push(chunkMayus);
    cb();
}

let mayus = new Mayus();
readableStream.pipe(mayus)
.pipe(process.stdout)