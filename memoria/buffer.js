//los buffer nos permiten trabajar con los datos en su estado mas crudo, sin manejar tipos, los datos se guardan en la memoria ram
//en binario y se muestran por consola en hexadecimal
//Los buffer por lo general se trabajan en conjunto con los streams

//esta funcion va a apartar cuatro espacios de memoria vacios
// let buffer = Buffer.alloc(4);

//Esta funcion crea y almacena los datos del array en espacios de memoria
// let buffer = Buffer.from([0, 1, 2, 3]);

let buffer = Buffer.from('Hola');

console.log(buffer);


//--

let abc = Buffer.alloc(26);
console.log(abc)

for(let i = 0; i<26; i++) {
    abc[i] = i + 97;
}

console.log(abc);
console.log(abc.toString());
